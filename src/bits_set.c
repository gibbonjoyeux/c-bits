/////////////////////////////// BY GIBBONJOYEUX ////////////////////////////////

#include "bits.h"

////////////////////////////////////////////////////////////////////////////////
/// PRIVATE FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
/// PUBLIC FUNCTION
////////////////////////////////////////////////////////////////////////////////

void		bits_set(
	void	*buffer,
	int		index,
	char	value) {
	char	*data;
	int		byte_index;
	char	bit_index;

	data = buffer;
	/// [1] COMPUTE BITS POSITION
	byte_index = index / 8;
	bit_index = index - byte_index * 8;
	/// [2] SET BIT
	if (value == 0)
		data[byte_index] = (data[byte_index] | (value << bit_index));
	else
		data[byte_index] = (data[byte_index] | (-1 & (value << bit_index)));
}
